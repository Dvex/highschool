class AssistsController < ApplicationController
  before_filter :authenticate_user!
  def index
    @now = Time.now
    if current_user.type == 'Auxiliary'
      sectionDetail = SectionDegreeDetail.where(:auxiliary_id => current_user.id).first
      if !sectionDetail.nil?
        @section = sectionDetail.sections_degree
      else
        @message = 'Este auxiliar no esta asociado a ninguna sección para registrar asistencia.'
      end
      if Assist.where(:created_at => @now.beginning_of_day..@now.end_of_day).count < 1
        @flag = true
        @message_now = 'Ya se tomó las asistencias hoy ' + @now.strftime('%d/%m/%Y')
      end
    else
      @message_error = 'Usted no puede registrar las asistencias.'
    end
  end

  def show
    @assist = SectionsDegree.find(params[:id]).assists.first
  end

  def history
    @assists = SectionsDegree.find(params[:id]).assists
  end

  def particulary_assist
    @assist = Array.new
    SectionsDegree.find(params[:id]).assists.each do |assist|
      @assist = assist.assist_details.where(:student_id => params[:student_id])
    end
  end

  def new
    @assist = Assist.new
    @now = Time.now
    @reg_n = ((@now.to_f)*100).to_i
    section_degree = SectionDegreeDetail.where(:auxiliary_id => current_user.id).first.sections_degree
    @students = Student.where(:pay => 1).where(:sections_degree_id => section_degree.id)
  end

  def create
    assist = Assist.new(assist_params)
    assist.save
    flash[:notice] = "Se ha registrado correctamente las asistencias."
    redirect_to :action => :index
  end

  def edit
    @action = 'edit'
    @now = Time.now
    @reg_n = ((@now.to_f)*100).to_i
    @assist = Assist.find(params[:id])
  end

  def update
    assist = Assist.find(params[:id]).update_attributes(assist_params)
    flash[:notice] = "Se ha actualizado correctamente las asistencias."
    redirect_to :action => :index
  end

  def destroy
    assist = Assist.find(params[:id])
    redirect_to :action => :index
  end

  def assist_params
    params.require(:assist).permit(
      :name,
      :description,
      :sections_degree_id,
      assist_details_attributes: [
        :id,
        :assist_id,
        :student_id,
        :attended,
        :belate,
        :comentary
      ]
    )
  end
end
