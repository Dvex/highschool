class AuxiliariesController < ApplicationController
  before_filter :authenticate_user!
  def index
    @auxiliaries = Auxiliary.all.order(:first_name)
  end

  def show
    @auxiliary = Auxiliary.find(params[:id])
  end

  def new
    @auxiliary = Auxiliary.new
    @time = Time.now.to_date
  end

  def create
    auxiliary = Auxiliary.new(params_auxiliary)
    generated_passw = Devise.friendly_token.first(8)
    auxiliary.email = auxiliary.first_name.to_s.downcase[0] + '.' + auxiliary.paternal_surname.to_s + '@loremipsum.com'
    auxiliary.password = generated_passw
    auxiliary.password_confirmation = generated_passw
    auxiliary.roles = [:assistant]
    if auxiliary.save
      UserMailer.send_new_user_message(auxiliary, generated_passw).deliver_now
      flash[:notice] = "Auxiliar #{auxiliary.first_name.to_s + ' ' + auxiliary.second_name.to_s + ' ' + auxiliary.paternal_surname.to_s + ' ' + auxiliary.maternal_surname.to_s} registrado."
    else
      if auxiliary.errors.added?(:email, :taken)
        auxiliary.email = auxiliary.first_name.to_s.downcase[0] + auxiliary.second_name.to_s.downcase[0] + '.' + auxiliary.paternal_surname.to_s + '@loremipsum.com'
        if auxiliary.save
          UserMailer.send_new_user_message(auxiliary, generated_passw).deliver_now
          flash[:notice] = "Alumno #{auxiliary.first_name.to_s + ' ' + auxiliary.second_name.to_s + ' ' + auxiliary.paternal_surname.to_s + ' ' + auxiliary.maternal_surname.to_s} Matriculado."
        else
          flash[:notice] = "Este auxiliar tiene los mismos nombres y primer apellido iguales a otro ya registrado. ¿Está seguro del registro?."
        end
      end
    end
    redirect_to :action => :index
  end

  def edit
    @auxiliary = Auxiliary.find(params[:id])
  end

  def update
    auxiliary = Auxiliary.find(params[:id]).update_attributes(params_auxiliary)
    redirect_to :action => :index
  end

  def destroy
    @auxiliary = Auxiliary.destroy(params[:id])
    redirect_to :action => :index
  end

  private
  def params_auxiliary
    params.require(:auxiliary).permit(
      :first_name, 
      :second_name, 
      :paternal_surname, 
      :maternal_surname, 
      :date_of_birth, 
      :owner_email,
      :dni, 
      :phone, 
      :address
    )
  end
end
