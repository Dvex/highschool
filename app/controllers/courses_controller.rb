class CoursesController < ApplicationController
  before_filter :authenticate_user!
  def index
    @courses = Course.where(:sections_degree_id => nil).order('name ASC')
  end

  def new
    @action = 'new'
    @course = Course.new
  end

  def addMoreRows
    @reg_n = ((Time.now.to_f)*100).to_i
    render(:partial => 'form_row', :layout => false)
  end

  def create
    course = Course.new(params_course)
    if course.save
      SectionsDegree.select(:id).select(:name).select(:degree_id).each do |sectionDegree|
        i_course = Course.new
        i_course.name = course.name.to_s + ' - ' + sectionDegree.degree.name.to_s + ' ' + sectionDegree.name.to_s
        i_course.sections_degree_id = sectionDegree.id
        i_course.course_id = course.id
        i_course.save
      end
    end
    redirect_to :action => :index
  end

  def edit
    @reg_n = ((Time.now.to_f)*100).to_i
    @action = 'edit'
    @course = Course.find(params[:id])
  end

  def update
    course = Course.find(params[:id]).update_attributes(params_course)
    redirect_to :action => :index
  end

  def destroy
    @course = Course.find(params[:id])
    @course.course_details.destroy_all
    @course.destroy
    redirect_to :action => :index
  end

  private
  def params_course
    params.require(:course).permit(
      :name,
      :silabus,
      course_details_attributes: [
        :id,
        :course_id,
        :name,
        :_destroy
      ]
    )
  end
end
