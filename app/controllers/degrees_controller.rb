class DegreesController < ApplicationController
  before_filter :authenticate_user!
  def index
    @degrees = Degree.order('name ASC')
  end

  def new
    @degree = Degree.new
  end

  def create
    degree = Degree.new(params_degree)
    degree.save
    redirect_to :action => :index
  end

  def edit
    @degree = Degree.find(params[:id])
  end

  def update
    degree = Degree.find(params[:id]).update_attributes(params_degree)
    redirect_to :action => :index
  end

  def destroy
    degree = Degree.destroy(params[:id])
    redirect_to :action => :index
  end

  private
  def params_degree
    params.require(:degree).permit(
      :name
    )
  end
end
