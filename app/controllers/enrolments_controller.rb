class EnrolmentsController < ApplicationController
  before_filter :authenticate_user!
  def index
    @enrolments = Student.all
  end

  def new
    @student = Student.new
    @sectionsDegrees = SectionsDegree.all
    @reg_n = ((Time.now.to_f)*100).to_i
  end

  def create
    @student = Student.new(student_params)
    generated_passw = Devise.friendly_token.first(8)
    @student.email = @student.first_name.to_s.downcase[0] + '.' + @student.paternal_surname.to_s + '@loremipsum.com'
    @student.password = generated_passw
    @student.password_confirmation = generated_passw
    @student.roles = [:student]
    if @student.save
      if @student.pay?
        @student.update_attribute(:email_sent, 1)
        UserMailer.send_new_user_message_enrollment(@student, generated_passw).deliver
      end
      flash[:notice] = "Alumno #{@student.first_name.to_s + ' ' + @student.second_name.to_s + ' ' + @student.paternal_surname.to_s + ' ' + @student.maternal_surname.to_s} Matriculado."
    else
      if @student.errors.added?(:email, :taken)
        @student.email = @student.first_name.to_s.downcase[0] + @student.second_name.to_s.downcase[0] + '.' + @student.paternal_surname.to_s + '@loremipsum.com'
        if @student.save
          if @student.pay?
            @student.update_attribute(:email_sent , 1)
            UserMailer.send_new_user_message_enrollment(@student, generated_passw).deliver
          end
          flash[:notice] = "Alumno #{@student.first_name.to_s + ' ' + @student.second_name.to_s + ' ' + @student.paternal_surname.to_s + ' ' + @student.maternal_surname.to_s} Matriculado."
        else
          flash[:notice] = "Este alumno tiene los mismos nombres y primer apellido iguales a otro ya registrado. ¿Está seguro del registro?."
        end
      end
    end
    redirect_to :action => :index
  end

  def edit
    @student = Student.find(params[:id])
    @sectionsDegrees = SectionsDegree.all
    @reg_n = ((Time.now.to_f)*100).to_i
  end

  def update
    student = Student.find(params[:id])
    @student = student.update_attributes(student_params)
    if @student
      if student.pay?
        if !student.email_sent?
          generated_passw = Devise.friendly_token.first(8)
          if student.update_attributes(:password => generated_passw, :password_confirmation => generated_passw, :email_sent => 1)
            UserMailer.send_new_user_message_enrollment(student, generated_passw).deliver
          end
        end
      end
    end
    redirect_to :action => :index
  end

  def destroy
    Student.find(params[:id]).student_details.destroy_all
    @student = Student.destroy(params[:id])
    redirect_to :action => :index
  end

  private
  def student_params
    params.require(:student).permit(
      :first_name, 
      :second_name, 
      :paternal_surname, 
      :maternal_surname, 
      :date_of_birth, 
      :last_grade, 
      :dni, 
      :sections_degree_id,
      :pay,
      student_details_attributes: [
        :id,
        :student_id,
        :representative_first_name,
        :representative_second_name,
        :representative_paternal_surname,
        :representative_maternal_surname,
        :representative_email_1,
        :representative_email_2,
        :place_work_1,
        :phone_work_1,
        :representative_celphone_1,
        :dni_1,
        :second_representative_first_name,
        :second_representative_second_name,
        :second_representative_paternal_surname,
        :second_representative_maternal_surname,
        :second_representative_email_1,
        :second_representative_email_2,
        :place_work_2,
        :phone_work_2,
        :representative_celphone_2,
        :dni_2,
        :celphone,
        :address,
        :second_address,
        :health_insurance,
        :health_phone,
        :affiliate_number
      ]
    )
  end
end
