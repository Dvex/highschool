class GlobalConfigurationsController < ApplicationController
  def putsNotes
    @teachers = Teacher.all
    @auxiliaries = Auxiliary.all
    @sections_array = Array.new
  end

  def update
    if !params[:teacher_oauths].nil?
      Teacher.where(:id => params[:teacher_oauths]).each do |to|
        to.update_attributes(:put_note => true)
      end
      Teacher.where.not(:id => params[:teacher_oauths]).each do |to|
        to.update_attributes(:put_note => false)
      end
    elsif ![:auxiliary_oauths].nil?
      Auxiliary.where(:id => params[:auxiliary_oauths]).each do |ao|
        ao.update_attributes(:put_note => true)
      end
      Auxiliary.where.not(:id => params[:auxiliary_oauths]).each do |ao|
        ao.update_attributes(:put_note => false)
      end
    end

    redirect_to :action => :putsNotes
  end
end
