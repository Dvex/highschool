class GradesController < ApplicationController
  def index
    lists = TimetableDetail.where("teacher_id = ?", current_user.id)
    @sectionsdegree = Array.new
    lists.each do |l|
      @sectionsdegree << Timetable.find(l.timetable_id).sections_degree_id
    end
    @sectionsdegree = @sectionsdegree.uniq
  end

  def put_grade
    @time = Time.now.to_date
    @course_details = CourseDetail.where("course_id = ?",params[:id])
    @reg_n = ((Time.now.to_f)*100).to_i
    timetable = Timetable.find(params[:timetable_id])
    @course = params[:id]
    @grade = Grade.new
    @students = User.where("type LIKE 'Student' AND sections_degree_id = ?",timetable.sections_degree_id)
  end

  def create
    params[:grade_details_attributes].each do |gd|
      grade = Grade.new
      grade.name = params[:grade]['name']
      grade.date_of_issue = params[:grade]['date_of_issue']
      grade.course_detail_id = params[:grade]['course_detail_id']
      grade.student_id = gd[1]['student_id']
      grade.grade = gd[1]['grade']
      grade.save
    end
    redirect_to :action => :index
  end

  def show_list_students
    @students = User.where("type LIKE 'Student'")
  end

  def show_grades_student
    @student = User.find(params[:id])
    @courses = Array.new
    @period = Period.where("start_date <= ? AND end_date >= ?", Time.now.to_date, Time.now.to_date).first
    timetable = Timetable.where("sections_degree_id = ?",@student.sections_degree_id).first
    timetable.timetable_details.each do |ttd|
      @courses << Course.find(ttd.course_id)
    end
  end

  def show_report
    @student = User.find(params[:id])
    periods = Period.where("status = 1")
    @title_array = Array.new
    @title_array << "Cursos"
    periods.each do |per|
      @title_array << per.name.to_s
    end
    courses = Array.new
    timetable = Timetable.where("sections_degree_id = ?",@student.sections_degree_id).first
    timetable.timetable_details.each do |ttd|
      courses << ttd.course_id
    end
    @grades_array = Array.new
    courses.each do |cos|
      cour = Course.find(cos)
      course_string = cour.name.to_s
      periods.each do |per|
        period_grade = CourseGrade.where("course_id = ? AND period_id = ?",cos,per.id).first
        if period_grade.nil?
          course_string = course_string + ',' + '-'
        else
          course_string = course_string + ',' + period_grade.grade.to_s
        end
      end
      @grades_array << course_string.split(",")
    end
  end

  def student_grades_pdf
    @student = User.find(params[:id])
    periods = Period.where("status = 1")
    @title_array = Array.new
    @title_array << "Cursos"
    @titlewidth=100
    @titlecolumns = 100.to_s
    periods.each do |per|
      @title_array << per.name.to_s
      @titlewidth+=125
      @titlecolumns=@titlecolumns+','+ 125.to_s
    end
    courses = Array.new
    timetable = Timetable.where("sections_degree_id = ?",@student.sections_degree_id).first
    timetable.timetable_details.each do |ttd|
      courses << ttd.course_id
    end
    @grades_array = Array.new
    courses.each do |cos|
      cour = Course.find(cos)
      course_string = cour.name.to_s
      periods.each do |per|
        period_grade = CourseGrade.where("course_id = ? AND period_id = ?",cos,per.id).first
        if period_grade.nil?
          course_string = course_string + ',' + '-'
        else
          course_string = course_string + ',' + period_grade.grade.to_s
        end
      end
      @grades_array << course_string.split(",")
    end
    puts @grades_array.inspect
    prawnto inline: true, :prawn => { :page_size => 'A4', :page_layout => :portrait }
    render layout: false
  end

  def complete
    @combo=Array.new
    @courses = TimetableDetail.where("teacher_id = ? AND timetable_id = ?",current_user.id,Timetable.where("sections_degree_id = ?", params[:chosen]).first.id)
    @courses.each do |x|
      @combo << { 'id' => x.course_id, 'name' => Course.find(x.course_id).name.to_s, 'timetable_id' => x.timetable_id}
    end
    render json: {:combo => @combo}
  end

end
