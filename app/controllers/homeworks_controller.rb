class HomeworksController < ApplicationController
  before_filter :authenticate_user!
  def index
    @homeworks = Homework.where("status = 1")
  end

  def new
    @time = Time.now.to_date
    lists = TimetableDetail.where("teacher_id = ?", current_user.id)
    @sectionsdegree = Array.new
    lists.each do |l|
      @sectionsdegree << Timetable.find(l.timetable_id).sections_degree_id
    end
    @sectionsdegree = @sectionsdegree.uniq
    @homework = Homework.new
  end

  def create
    homework = Homework.new(params_homework)
    homework.status = 1
    homework.save
    redirect_to :action => :index
  end

  def edit
    @action = 'edit'
    @lists = TimetableDetail.where("teacher_id = ?", current_user.id)
    @reg_n = ((Time.now.to_f)*100).to_i
    @homework = Homework.find(params[:id])
  end

  def update
    homework = Homework.find(params[:id]).update_attributes(params_homework)
    redirect_to :action => :index
  end

  def destroy
    homework = Homework.destroy(params[:id])
    redirect_to :action => :index
  end

  def desactivate
    homework = Homework.find(params[:id])
    homework.status = 0
    homework.save
    redirect_to :action => :index
  end

  def particulary_homework
    @homeworks = Homework.all
  end

  def complete
    @combo=Array.new
    @courses = TimetableDetail.where("teacher_id = ? AND timetable_id = ?",current_user.id,Timetable.where("sections_degree_id = ?", params[:chosen]).first.id)
    @courses.each do |x|
      @combo << { 'id' => x.course_id, 'name' => Course.find(x.course_id).name.to_s}
    end
    render json: {:combo => @combo}
  end

  private
  def params_homework
    params.require(:homework).permit(
      :title,
      :description,
      :date_of_issue,
      :course_id,
      :sections_degree_id,
      :attachment
    )
  end
end
