class MainController < ApplicationController
  before_filter :authenticate_user!
  def index
    if current_user.type == 'Student'
      @notices = Notice.where("important = 1 AND date_of_issue > ?",Time.now.to_date).first
      if !@notices.nil?
        @count_notices = 1
      end
      @global_notices = Notice.where(:global_notice => 1).take(10)
      if !current_user.type.nil?
        @individual_notices = Array.new
        current_user.notice_details.take(10).each do |d|
          @individual_notices << d.notice
        end
      end
    end
  end

end
