class NoticesController < ApplicationController
  before_filter :authenticate_user!
  def index
    @notices = Notice.all
  end

  def new
    @notice = Notice.new
  end

  def show
    @notice = Notice.find(params[:id])
  end

  def show_all
    if !current_user.type.nil?
      @individual_notices = Array.new
      current_user.notice_details.take(10).each do |d|
        @individual_notices << d.notice
      end
    end
    @notices_important = Notice.where(:important => 1)
    @notices_global = Notice.where(:global_notice => 1)
  end

  def addRow
    @reg_n = ((Time.now.to_f)*100).to_i
    if params[:teacher_selected].to_i != 0
      @teachers = Teacher.all
    end
    if params[:student_selected].to_i != 0
      @students = Student.all
    end
    if params[:auxiliary_selected].to_i != 0
      @auxiliaries = Auxiliary.all
    end
    render(:partial => 'form_row', :layout => false)
  end

  def create
    notice = Notice.new(params_notice)
    notice.save
    notice.notice_details.each do |nd|
      if nd.student_id != nil
        UserMailer.send_notice_to_user(nd.student_id,nd.notice_id).deliver
      end
    end
    redirect_to :action => :index
  end

  def edit
    @action = 'edit'
    @reg_n = ((Time.now.to_f)*100).to_i
    @notice = Notice.find(params[:id])
  end

  def update
    notice = Notice.find(params[:id]).update_attributes(params_notice)
    redirect_to :action => :index
  end

  def destroy
    notice = Notice.destroy(params[:id])
    redirect_to :action => :index
  end

  private
  def params_notice
    params.require(:notice).permit(
      :title, 
      :description,
      :attachment,
      :global_notice,
      :important,
      :date_of_issue,
      notice_details_attributes: [
        :id,
        :notice_id,
        :student_id,
        :auxiliary_id,
        :teacher_id,
        :_destroy
      ]
    )
  end
end
