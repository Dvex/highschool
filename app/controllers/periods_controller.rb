class PeriodsController < ApplicationController
  before_filter :authenticate_user!
  def index
    @periods = Period.all
  end

  def new
    @period = Period.new
    if Period.last.nil?
      @lastperiod = ''
    else
      @lastperiod = Period.last.end_date
    end
  end

  def create
    period = Period.new(params_period)
    period.status = 1
    period.grade_flag = 1
    period.save
    redirect_to :action => :index
  end

  def edit
    @period = Period.find(params[:id])
  end

  def update
    period = Period.find(params[:id]).update_attributes(params_period)
    redirect_to :action => :index
  end

  def destroy
    period = Period.destroy(params[:id])
    redirect_to :action => :index
  end

  def get_grades
    period = Period.find(params[:id])
    timetables = Timetable.all
    timetables.each do |tt|
      numberplaces = 1
      course_array = Array.new
      tt.timetable_details.each do |ttd|
        course_array << ttd.course_id
      end
      students = User.where("type LIKE 'Student' AND sections_degree_id = ?", tt.sections_degree_id)
      students.each do |st|
        course_array.each do |co|
          course = Course.find(co)
          course_detail_array = Array.new
          course.course_details.each do |cod|
            course_detail_array << cod.id
            cdg = CourseDetailGrade.new
            cdg.date_of_issue = Time.now.to_date
            cdg.course_id = co
            cdg.course_detail_id = cod.id
            cdg.student_id = st.id
            cdg.period_id = period.id
            period_grade = Grade.where("student_id = ? AND course_detail_id = ? AND date_of_issue BETWEEN ? AND ?", st.id, cod.id, period.start_date, period.end_date).average(:grade)
            if period_grade != nil
              cdg.grade = period_grade.round(2)
            else
              cdg.grade = 0
            end
            cdg.save
          end
          cgrade = CourseGrade.new
          cgrade.date_of_issue = Time.now.to_date
          cgrade.course_id = co
          cgrade.student_id = st.id
          cperiod_grade = CourseDetailGrade.where("student_id = ? AND course_id = ? AND date_of_issue BETWEEN ? AND ?", st.id, co, period.start_date, period.end_date).average(:grade)
          if cperiod_grade != nil
            cgrade.grade = cperiod_grade.round(2)
          else
            cgrade.grade = 0
          end
          cgrade.period_id = period.id
          cgrade.save
        end
        places = StudentPlace.new
        places.user_id = st.id
        places.sections_degree_id = tt.sections_degree_id
        places.gradeaverage = (CourseGrade.where("student_id = ? AND period_id = ?", st.id, period.id).average(:grade)).to_f
        places.period_id = period.id
        places.save
      end
      orderedplaces = ActiveRecord::Base.connection.execute("
        SELECT id 
        FROM student_places 
        WHERE sections_degree_id = '"+tt.sections_degree_id.to_s+"'
        ORDER BY gradeaverage DESC
      ")
      orderedplaces.each do |op|
        orderpla = StudentPlace.find(op[0])
        orderpla.place = numberplaces
        orderpla.save
        numberplaces = numberplaces + 1
      end
    end
    period.grade_flag = 0
    period.save
    redirect_to :action => :index
  end

  def desactivate
    period = Period.find(params[:id])
    period.status = 0
    period.save
    redirect_to :action => :index
  end

  private
  def params_period
    params.require(:period).permit(
      :name,
      :start_date,
      :end_date
    )
  end
end
