class SectionsDegreesController < ApplicationController
  before_filter :authenticate_user!
  def index
    @degrees = Degree.all
  end

  def new
    @degrees = Degree.all
    @sectionsDegree = SectionsDegree.new
  end

  def addAuxiliaries
    @auxiliaries = Auxiliary.all
    @reg_n = ((Time.now.to_f)*100).to_i
    render(:partial => 'row_table', :layout => false)
  end

  def create
    sectionsDegree = SectionsDegree.new(params_section_degree)
    sectionsDegree.save
    redirect_to :action => :index
  end

  def edit
    @action = 'edit'
    @degrees = Degree.all
    @auxiliaries = Auxiliary.all
    @sectionsDegree = SectionsDegree.find(params[:id])
    @reg_n = ((Time.now.to_f)*100).to_i
  end

  def update
    sectionsDegree = SectionsDegree.find(params[:id]).update_attributes(params_section_degree)
    redirect_to :action => :index
  end

  def destroy
    sectionsDegree = SectionsDegree.destroy(params[:id])
    redirect_to :action => :index
  end

  private
  def params_section_degree
    params.require(:sections_degree).permit(
      :name,
      :degree_id,
      section_degree_details_attributes: [
        :id,
        :sections_degree_id,
        :auxiliary_id,
        :_destroy
      ]
    )
  end
end
