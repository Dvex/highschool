class TeachersController < ApplicationController
  before_filter :authenticate_user!
  def index
    @teachers = Teacher.all.order(:first_name)
  end

  def new
    @teacher = Teacher.new
    @time = Time.now.to_date
  end

  def create
    teacher = Teacher.new(params_teacher)
    generated_passw = Devise.friendly_token.first(8)
    teacher.email = teacher.first_name.to_s.downcase[0] + '.' + teacher.paternal_surname.to_s + '@loremipsum.com'
    teacher.password = generated_passw
    teacher.password_confirmation = generated_passw
    teacher.roles = [:teacher]
    if teacher.save
      UserMailer.send_new_user_message(teacher,generated_passw).deliver_now
      flash[:notice] = "Profesor #{teacher.first_name.to_s + ' ' + teacher.second_name.to_s + ' ' + teacher.paternal_surname.to_s + ' ' + teacher.maternal_surname.to_s} registrado."
    else
      if teacher.errors.added?(:email, :taken)
        teacher.email = teacher.first_name.to_s.downcase[0] + teacher.second_name.to_s.downcase[0] + '.' + teacher.paternal_surname.to_s + '@loremipsum.com'
        if teacher.save
          UserMailer.send_new_user_message(teacher,generated_passw).deliver_now
          flash[:notice] = "Alumno #{teacher.first_name.to_s + ' ' + teacher.second_name.to_s + ' ' + teacher.paternal_surname.to_s + ' ' + teacher.maternal_surname.to_s} Matriculado."
        else
          flash[:notice] = "Este Profesor tiene los mismos nombres y primer apellido iguales a otro ya registrado. ¿Está seguro del registro?."
        end
      end
    end
    redirect_to :action => :index
  end

  def edit
    @teacher = Teacher.find(params[:id])
  end

  def update
    teacher = Teacher.find(params[:id]).update_attributes(params_teacher)
    redirect_to :action => :index
  end

  def destroy
    @teacher = Teacher.destroy(params[:id])
    redirect_to :action => :index
  end

  private
  def params_teacher
    params.require(:teacher).permit(
      :first_name, 
      :second_name, 
      :paternal_surname, 
      :maternal_surname, 
      :date_of_birth, 
      :owner_email,
      :dni, 
      :phone, 
      :address
    )
  end
end
