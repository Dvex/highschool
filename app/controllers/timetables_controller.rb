class TimetablesController < ApplicationController
  before_filter :authenticate_user!
  def index
    @timetables = Timetable.all
  end

  def show
    @tbl = Array.new
    @timetable = Timetable.find(params[:id])
    @days = Date::DAYNAMES.map{|s| I18n.t s}
    @days.shift
    @hours = @timetable.timetable_details.select(:start_hour).select(:end_hour).order(:start_hour).uniq
    @timetable.timetable_details.each do |detail|
      if !detail.teacher.nil?
        @tbl << [ detail.teacher.paternal_surname.to_s + ' ' + detail.teacher.maternal_surname.to_s + ', ' + detail.teacher.first_name.to_s + ' ' + detail.teacher.second_name.to_s, detail.teacher.owner_email.to_s, detail.course.name.to_s]
      end
    end
    @tbl.uniq!
  end

  def new
    @timetable = Timetable.new
    @sectionDegrees = SectionsDegree.all
    
  end

  def addMoreRows
    @reg_n = ((Time.now.to_f)*100).to_i
    @courses = Course.where(:sections_degree_id => params[:section_id])
    @teachers = Teacher.all
    @days = Date::DAYNAMES.map{|s| I18n.t s}
    render(:partial => 'form_row', :layout => false)
  end

  def create
    timetable = Timetable.new(timetable_params)
    timetable.save
    flash[:notice] = "Se ha creado correctamente el horario."
    redirect_to :action => :index
  end

  def edit
    @action = 'edit'
    @reg_n = ((Time.now.to_f)*100).to_i
    @sectionDegrees = SectionsDegree.all
    @timetable = Timetable.find(params[:id])
    @courses = Course.all
    @teachers = Teacher.all
    @days = Date::DAYNAMES.map{|s| I18n.t s}
  end

  def update
    timetable = Timetable.find(params[:id]).update_attributes(timetable_params)
    flash[:notice] = "Se ha actualizado correctamente el horario."
    redirect_to :action => :index
  end

  def destroy
    timetable = Timetable.find(params[:id])
    timetable.timetable_details.destroy_all
    timetable.destroy
    flash[:notice] = "Se eliminó correctamente el horario."
    redirect_to :action => :index
  end

  def complete
    @combo=Array.new
    @teacher = Teacher.all
    @teacher.each do |x|
      @combo << { 'id' => x.id, 'name' => x.first_name.to_s + ' ' + x.paternal_surname.to_s + ' ' + x.maternal_surname.to_s}
    end
    render json: {:combo => @combo}
  end

  private
  def timetable_params
    params.require(:timetable).permit(
      :name, 
      :sections_degree_id,
      timetable_details_attributes: [
        :id,
        :timetable_id,
        :course_id,
        :teacher_id,
        :day,
        :start_hour,
        :end_hour,
        :_destroy
      ]
    )
  end
end
