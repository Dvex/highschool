class UsersController < ApplicationController
  before_filter :authenticate_user!
  def edit
    @user = current_user
  end

  def update_password
    @user = User.find(current_user.id)
    if @user.update(user_params)
      sign_in @user, :bypass => true
      flash[:notice] = "Contraseña actualizada correctamente."
      redirect_to root_path
    else
      render "edit"
    end
  end

  def user_params
    params.require(:student).permit(:password, :password_confirmation)
  end
end
