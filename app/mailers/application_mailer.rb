class ApplicationMailer < ActionMailer::Base
  default from: "deliverymessage@loremipsum.com"
  layout 'mailer'
end
