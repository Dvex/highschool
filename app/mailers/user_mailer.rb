class UserMailer < ApplicationMailer

  def send_new_user_message(user, pwd)
    @user = user
    @pwd = pwd
    mail(:to => @user.owner_email, :subject => "Su cuenta de correo al sistema se ha creado correctamente.")
  end

  def send_new_user_message_enrollment(user, pwd)
    @user = user
    @pwd = pwd
    if !@user.student_details.first.representative_email_1.nil?
      to = @user.student_details.first.representative_email_1
    elsif !@user.student_details.first.second_representative_email_1.nil?
      to = @user.student_details.first.second_representative_email_1
    end
    mail(:to => to, :subject => "Su cuenta de correo al sistema se ha creado correctamente.")	
  end

  def send_notice_to_user(user, notice)
    @user = User.find(user)
    @notice = Notice.find(notice)
    mail(:to => @user.owner_email, :subject => "Hay un nuevo aviso")
  end
end
