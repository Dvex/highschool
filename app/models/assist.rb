class Assist < ActiveRecord::Base
	has_many :assist_details
	belongs_to :sections_degree
	accepts_nested_attributes_for :assist_details
end
