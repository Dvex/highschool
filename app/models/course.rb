class Course < ActiveRecord::Base
	has_and_belongs_to_many :degrees
	has_many :timetable_details
	has_many :course_details
	has_many :homeworks
	belongs_to :sections_degree

	accepts_nested_attributes_for :course_details, :allow_destroy => true

	has_attached_file :silabus, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
	do_not_validate_attachment_file_type :silabus
	#validates_attachment_content_type :silabus, :content_type => /\Aimage\/.*\Z/
end
