class Notice < ActiveRecord::Base
	has_many :notice_details
	has_attached_file :attachment, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
	validates_attachment_content_type :attachment, :content_type => /\Aimage\/.*\Z/
	
	accepts_nested_attributes_for :notice_details, :allow_destroy => true
end
