class NoticeDetail < ActiveRecord::Base
	belongs_to :notice
	belongs_to :teachers
	belongs_to :auxiliaries
	belongs_to :students
end
