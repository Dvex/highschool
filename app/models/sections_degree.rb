class SectionsDegree < ActiveRecord::Base
	has_many :students
	has_many :section_degree_details
	has_one :timetable
	has_many :assists
	has_many :courses
	belongs_to :degree
	
	accepts_nested_attributes_for :section_degree_details, :allow_destroy => true
end
