class Student < User
  has_many :student_details
  has_many :assist_details
  has_many :notice_details
  belongs_to :sections_degree
  belongs_to :notice

  accepts_nested_attributes_for :student_details
end
