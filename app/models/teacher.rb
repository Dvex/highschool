class Teacher < User
  has_many :timetable_details
  has_many :notice_details
  belongs_to :notice

end
