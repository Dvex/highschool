class Timetable < ActiveRecord::Base
	has_many :timetable_details
	belongs_to :sections_degree

	accepts_nested_attributes_for :timetable_details, :allow_destroy => true
end
