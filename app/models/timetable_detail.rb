class TimetableDetail < ActiveRecord::Base
	belongs_to :timetable
	belongs_to :course
	belongs_to :teacher
end
