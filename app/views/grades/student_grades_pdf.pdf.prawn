bounding_box [bounds.left + 100, bounds.bottom + 788], :width  => bounds.width do
  table([ ["Colegio Lorem Ipsum"] ], :width => 320, :cell_style => {:height => 64}, :column_widths => [320]) do
          style(row(0), :valign => :center)
          style(column(0), :align => :center)
          style(columns(0), :size => 10)
          columns(0).font_style = :bold
        end
end
move_down 15

table([ ["GRADO Y SECCIÓN","AP. PATERNO", "AP. MATERNO", "NOMBRES"] ], :width => 470, :cell_style => {:height => 18}, :column_widths => [120,100,100,150]) do
          style(columns(0..3), :align => :center)
          style(columns(0..3), :size => 7)
          columns(0..3).font_style = :bold
          style(row(0), :background_color => '205081')
        end
table([ ["#{@student.sections_degree.degree.name.to_s} #{@student.sections_degree.name.to_s}", "#{@student.paternal_surname.to_s}", "#{@student.maternal_surname.to_s}","#{@student.first_name.to_s} #{@student.second_name.to_s}"] ], :width => 470, :cell_style => {:height => 18}, :column_widths => [120,100,100,150]) do
  style(columns(0..3), :align => :center)
  style(columns(0..3), :size => 7)
  style(columns(0..3), :valign => :center)
end
move_down 15
text "NOTAS", :size => 12
table([ 
  @title_array.each do |title|
  ["#{title}"]
  end
   ], :width => "#{@titlewidth}".to_i, :cell_style => {:height => 16}, :column_widths => [   "#{@titlecolumns}".to_i  ]) do
      style(columns(0..5), :align => :center)
      style(columns(0..5), :size => 7)
end

@grades_array.each do |grade|
    table([ 
    grade.each do |gra|
      ["#{gra}"]
    end
    ], :width => "#{@titlewidth}".to_i, :cell_style => {:height => 16}, :column_widths => [   "#{@titlecolumns}".to_i  ]) do
      style(columns(0..5), :align => :center)
      style(columns(0..5), :size => 7)
    end
end

move_down 20
table([ ["Periodo","Comentarios"] ], :width => 470, :cell_style => {:height => 18}, :column_widths => [120,350]) do
          style(columns(0..2), :align => :center)
          style(columns(0..2), :size => 7)
          columns(0..2).font_style = :bold
          style(row(0), :background_color => '205081')
        end

@title_array.shift
@title_array.each do |title|
    table([ ["#{title}",""] ], :width => 470, :cell_style => {:height => 18}, :column_widths => [120,350]) do
          style(columns(0..2), :align => :center)
          style(columns(0..2), :size => 7)
          columns(0..2).font_style = :bold
        end
end