Rails.application.routes.draw do

  get 'global_configurations/putsNotes'
  patch 'global_configurations/update'

  root 'main#index'

  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  resources :courses do
    collection do
      post 'addMoreRows'
    end
  end
  resources :periods do
    member do
      get 'desactivate'
      get 'get_grades'
    end
  end
  resources :homeworks do
    collection do
      post 'complete'
    end
    member do
      get 'desactivate'
      get 'particulary_homework'
    end
  end
  resources :notices do
    collection do
      get 'show_all'
      post 'addRow'
    end
  end
  resources :assists do
    member do
      post 'history'
      get 'particulary_assist'
    end
  end
  resources :teachers
  resources :auxiliaries
  resources :degrees
  resources :timetables do
    collection do
      post 'addMoreRows'
      post 'complete'
    end
  end
  resources :enrolments
  resources :sections_degrees do
    collection do
      post 'addAuxiliaries'
    end
  end
  resources :users, only: [:edit] do
    collection do
      patch 'update_password'
    end
  end
  resources :grades do
    collection do
      post 'complete'
    end
    member do
      get 'put_grade'
      get 'show_list_students'
      get 'show_grades_student'
      get 'show_report'
      get 'student_grades_pdf'
      post 'student_grades_pdf'
    end
  end

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
