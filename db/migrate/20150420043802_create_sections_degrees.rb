class CreateSectionsDegrees < ActiveRecord::Migration
  def change
    create_table :sections_degrees do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
