class CreateAssists < ActiveRecord::Migration
  def change
    create_table :assists do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
