class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.integer :teacher_id
      t.integer :user_id
      t.float :quantity
      t.string :title

      t.timestamps null: false
    end
  end
end
