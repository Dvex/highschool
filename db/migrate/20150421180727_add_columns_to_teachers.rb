class AddColumnsToTeachers < ActiveRecord::Migration
  def change
    add_column :teachers, :dni, :string
    add_column :teachers, :phone, :string
    add_column :teachers, :address, :text
  end
end
