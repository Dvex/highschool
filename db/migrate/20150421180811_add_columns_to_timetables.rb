class AddColumnsToTimetables < ActiveRecord::Migration
  def change
    add_column :timetables, :day, :string
    add_column :timetables, :hour, :string
  end
end
