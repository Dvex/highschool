class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :first_name
      t.string :second_name
      t.string :last_grade
      t.integer :age
      t.date :date_of_birth

      t.timestamps null: false
    end
  end
end
