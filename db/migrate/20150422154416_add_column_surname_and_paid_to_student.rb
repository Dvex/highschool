class AddColumnSurnameAndPaidToStudent < ActiveRecord::Migration
  def change
    add_column :students, :surnames, :string
    add_column :students, :paid, :boolean
  end
end
