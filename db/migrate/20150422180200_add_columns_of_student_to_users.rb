class AddColumnsOfStudentToUsers < ActiveRecord::Migration
  def change
    add_column :users, :first_name, :string
    add_column :users, :second_name, :string
    add_column :users, :paternal_surname, :string
    add_column :users, :maternal_surname, :string
    add_column :users, :date_of_birth, :date
    add_column :users, :last_grade, :string
    add_column :users, :dni, :string
    add_column :users, :pay, :boolean
  end
end
