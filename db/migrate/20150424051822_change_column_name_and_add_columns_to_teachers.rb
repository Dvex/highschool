class ChangeColumnNameAndAddColumnsToTeachers < ActiveRecord::Migration
  def change
    rename_column :teachers, :name, :first_name
    add_column :teachers, :second_name, :string
    add_column :teachers, :paternal_surname, :string
    add_column :teachers, :maternal_surname, :string
    add_column :teachers, :date_of_birth, :date
  end
end
