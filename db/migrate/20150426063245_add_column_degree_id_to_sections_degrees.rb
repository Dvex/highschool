class AddColumnDegreeIdToSectionsDegrees < ActiveRecord::Migration
  def change
    add_column :sections_degrees, :degree_id, :integer
  end
end
