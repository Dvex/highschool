class CreateCourseTimetables < ActiveRecord::Migration
  def change
    create_table :courses_timetables do |t|
      t.integer :course_id
      t.integer :timetable_id

      t.timestamps null: false
    end
  end
end
