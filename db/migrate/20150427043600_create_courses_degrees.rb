class CreateCoursesDegrees < ActiveRecord::Migration
  def change
    create_table :courses_degrees do |t|
      t.integer :course_id
      t.integer :degree_id

      t.timestamps null: false
    end
  end
end
