class AddColumnSectionsDegreeIdToTimetables < ActiveRecord::Migration
  def change
    add_column :timetables, :sections_degree_id, :integer
  end
end
