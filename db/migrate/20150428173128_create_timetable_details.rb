class CreateTimetableDetails < ActiveRecord::Migration
  def change
    create_table :timetable_details do |t|
      t.string :day
      t.integer :course_id
      t.integer :teacher_id
      t.time :start_hour
      t.time :end_hour

      t.timestamps null: false
    end
  end
end
