class AddColumnTimetableIdToTimetableDetails < ActiveRecord::Migration
  def change
    add_column :timetable_details, :timetable_id, :integer
  end
end
