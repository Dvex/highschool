class AddColumnOwnerEmailToUsers < ActiveRecord::Migration
  def change
    add_column :users, :owner_email, :string
  end
end
