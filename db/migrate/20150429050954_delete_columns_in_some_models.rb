class DeleteColumnsInSomeModels < ActiveRecord::Migration
  def change
    remove_column :teachers, :first_name, :string
    remove_column :teachers, :dni, :string
    remove_column :teachers, :phone, :string
    remove_column :teachers, :address, :text
    remove_column :teachers, :second_name, :string
    remove_column :teachers, :paternal_surname, :string
    remove_column :teachers, :maternal_surname, :string
    remove_column :teachers, :date_of_birth, :date
    remove_column :students, :first_name, :string
    remove_column :students, :second_name, :string
    remove_column :students, :last_grade, :string
    remove_column :students, :date_of_birth, :date
    remove_column :students, :age, :integer
    remove_column :students, :surnames, :string
    remove_column :students, :paid, :boolean
  end
end
