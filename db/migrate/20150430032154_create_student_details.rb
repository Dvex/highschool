class CreateStudentDetails < ActiveRecord::Migration
  def change
    create_table :student_details do |t|
      t.string :representative_first_name
      t.string :representative_second_name
      t.string :representative_paternal_surname
      t.string :representative_maternal_surname
      t.string :health_insurance

      t.timestamps null: false
    end
  end
end
