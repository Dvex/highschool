class AddColumnsToStudentDetails < ActiveRecord::Migration
  def change
    add_column :student_details, :representative_email_1, :string
    add_column :student_details, :representative_email_2, :string
    add_column :student_details, :celphone, :string
    add_column :student_details, :address, :text
    add_column :student_details, :health_phone, :string
    add_column :student_details, :affiliate_number, :string
  end
end
