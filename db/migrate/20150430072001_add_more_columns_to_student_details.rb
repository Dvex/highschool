class AddMoreColumnsToStudentDetails < ActiveRecord::Migration
  def change
    add_column :student_details, :place_work_1, :string
    add_column :student_details, :phone_work_1, :string
    add_column :student_details, :place_work_2, :string
    add_column :student_details, :phone_work_2, :string
    add_column :student_details, :second_representative_email_1, :string
    add_column :student_details, :second_representative_email_2, :string
    add_column :student_details, :second_representative_first_name, :string
    add_column :student_details, :second_representative_second_name, :string
    add_column :student_details, :second_representative_paternal_surname, :string
    add_column :student_details, :second_representative_maternal_surname, :string
  end
end
