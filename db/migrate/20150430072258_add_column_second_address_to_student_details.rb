class AddColumnSecondAddressToStudentDetails < ActiveRecord::Migration
  def change
    add_column :student_details, :second_address, :text
  end
end
