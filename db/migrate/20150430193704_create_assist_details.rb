class CreateAssistDetails < ActiveRecord::Migration
  def change
    create_table :assist_details do |t|
      t.integer :assist_id
      t.integer :student_id
      t.boolean :attended
      t.boolean :belate
      t.text :comentary

      t.timestamps null: false
    end
  end
end
