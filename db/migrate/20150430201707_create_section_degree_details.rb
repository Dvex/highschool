class CreateSectionDegreeDetails < ActiveRecord::Migration
  def change
    create_table :section_degree_details do |t|
      t.integer :sections_degree_id
      t.integer :auxiliary_id

      t.timestamps null: false
    end
  end
end
