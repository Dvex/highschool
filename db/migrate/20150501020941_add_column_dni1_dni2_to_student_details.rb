class AddColumnDni1Dni2ToStudentDetails < ActiveRecord::Migration
  def change
    add_column :student_details, :dni_1, :string
    add_column :student_details, :dni_2, :string
  end
end
