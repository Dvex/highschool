class AddColumnsCelphonesToStudentDetails < ActiveRecord::Migration
  def change
    add_column :student_details, :representative_celphone_1, :string
    add_column :student_details, :representative_celphone_2, :string
  end
end
