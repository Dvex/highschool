class AddColumnSilabusToCourses < ActiveRecord::Migration
  def change
    add_attachment :courses, :silabus
  end
end
