class AddColumnSectionsDegreeIdToAssists < ActiveRecord::Migration
  def change
    add_column :assists, :sections_degree_id, :integer
  end
end
