class AddColumnPutNoteToUsers < ActiveRecord::Migration
  def change
    add_column :users, :put_note, :boolean
  end
end
