class AddColumnEmailSentToUsers < ActiveRecord::Migration
  def change
    add_column :users, :email_sent, :boolean
  end
end
