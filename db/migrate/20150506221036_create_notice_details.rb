class CreateNoticeDetails < ActiveRecord::Migration
  def change
    create_table :notice_details do |t|
      t.integer :student_id
      t.integer :auxiliary_id
      t.integer :teacher_id

      t.timestamps null: false
    end
  end
end
