class AddColumnGlobalNoticeToNotices < ActiveRecord::Migration
  def change
    add_column :notices, :global_notice, :boolean
  end
end
