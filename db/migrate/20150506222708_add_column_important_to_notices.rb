class AddColumnImportantToNotices < ActiveRecord::Migration
  def change
    add_column :notices, :important, :boolean
  end
end
