class AddAttachmentToNotice < ActiveRecord::Migration
  def change
    add_attachment :notices, :attachment
  end
end
