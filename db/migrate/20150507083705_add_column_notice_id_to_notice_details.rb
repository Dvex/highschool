class AddColumnNoticeIdToNoticeDetails < ActiveRecord::Migration
  def change
    add_column :notice_details, :notice_id, :integer
  end
end
