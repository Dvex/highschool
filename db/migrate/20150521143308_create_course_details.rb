class CreateCourseDetails < ActiveRecord::Migration
  def change
    create_table :course_details do |t|
      t.string :name
      t.integer :course_id
      t.timestamps null: false
    end
  end
end
