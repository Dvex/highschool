class CreateGrades < ActiveRecord::Migration
  def change
    create_table :grades do |t|
      t.string :name
      t.date :date_of_issue
      t.integer :course_detail_id
      t.integer :student_id
      t.column :grade,        :float,   :limit => 25
      t.timestamps null: false
    end
  end
end
