class CreateCourseGrades < ActiveRecord::Migration
  def change
    create_table :course_grades do |t|
      t.date :date_of_issue
      t.integer :course_id
      t.integer :student_id
      t.column :grade,        :float,   :limit => 25

      t.timestamps null: false
    end
  end
end
