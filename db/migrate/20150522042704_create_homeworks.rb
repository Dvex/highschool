class CreateHomeworks < ActiveRecord::Migration
  def change
    create_table :homeworks do |t|
      t.string :name
      t.string :description
      t.date :date_of_issue
      t.attachment :attachment

      t.timestamps null: false
    end
  end
end
