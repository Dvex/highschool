class AddstatusTohomework < ActiveRecord::Migration
  def change
    add_column :homeworks, :status, :integer
    rename_column :homeworks, :name, :title
  end
end
