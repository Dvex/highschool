class Addstatustoperiodandcourses < ActiveRecord::Migration
  def change
    add_column :periods, :status, :integer
    add_column :course_grades, :period_id, :integer
    add_column :course_detail_grades, :period_id, :integer
  end
end
