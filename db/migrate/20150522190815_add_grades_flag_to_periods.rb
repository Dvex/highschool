class AddGradesFlagToPeriods < ActiveRecord::Migration
  def change
    add_column :periods, :grade_flag, :integer
  end
end
