class CreateStudentPlaces < ActiveRecord::Migration
  def change
    create_table :student_places do |t|
      t.integer :user_id
      t.integer :sections_degree_id
      t.integer :period_id
      t.integer :place
      t.column :gradeaverage,        :float,   :limit => 25

      t.timestamps null: false
    end
  end
end
