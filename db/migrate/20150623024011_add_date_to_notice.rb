class AddDateToNotice < ActiveRecord::Migration
  def change
    add_column :notices, :date_of_issue, :date
  end
end
