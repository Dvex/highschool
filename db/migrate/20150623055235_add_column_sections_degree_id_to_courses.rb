class AddColumnSectionsDegreeIdToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :sections_degree_id, :integer
  end
end
