class AddColumnCourseIdToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :course_id, :integer
  end
end
