# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150430201707) do

  create_table "assist_details", force: :cascade do |t|
    t.integer  "assist_id",  limit: 4
    t.integer  "student_id", limit: 4
    t.boolean  "attended",   limit: 1
    t.boolean  "belate",     limit: 1
    t.text     "comentary",  limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "assists", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.text     "description", limit: 65535
  end

  create_table "auxiliaries", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "courses", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.text     "description", limit: 65535
  end

  create_table "courses_degrees", force: :cascade do |t|
    t.integer  "course_id",  limit: 4
    t.integer  "degree_id",  limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "courses_timetables", force: :cascade do |t|
    t.integer  "course_id",    limit: 4
    t.integer  "timetable_id", limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "degrees", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "notices", force: :cascade do |t|
    t.string   "title",       limit: 255
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "ratings", force: :cascade do |t|
    t.integer  "teacher_id", limit: 4
    t.integer  "user_id",    limit: 4
    t.float    "quantity",   limit: 24
    t.string   "title",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "section_degree_details", force: :cascade do |t|
    t.integer  "sections_degree_id", limit: 4
    t.integer  "auxiliary_id",       limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "sections_degrees", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "degree_id",  limit: 4
  end

  create_table "student_details", force: :cascade do |t|
    t.string   "representative_first_name",              limit: 255
    t.string   "representative_second_name",             limit: 255
    t.string   "representative_paternal_surname",        limit: 255
    t.string   "representative_maternal_surname",        limit: 255
    t.string   "health_insurance",                       limit: 255
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.integer  "student_id",                             limit: 4
    t.string   "representative_email_1",                 limit: 255
    t.string   "representative_email_2",                 limit: 255
    t.string   "celphone",                               limit: 255
    t.text     "address",                                limit: 65535
    t.string   "health_phone",                           limit: 255
    t.string   "affiliate_number",                       limit: 255
    t.string   "place_work_1",                           limit: 255
    t.string   "phone_work_1",                           limit: 255
    t.string   "place_work_2",                           limit: 255
    t.string   "phone_work_2",                           limit: 255
    t.string   "second_representative_email_1",          limit: 255
    t.string   "second_representative_email_2",          limit: 255
    t.string   "second_representative_first_name",       limit: 255
    t.string   "second_representative_second_name",      limit: 255
    t.string   "second_representative_paternal_surname", limit: 255
    t.string   "second_representative_maternal_surname", limit: 255
    t.text     "second_address",                         limit: 65535
  end

  create_table "students", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "teachers", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "timetable_details", force: :cascade do |t|
    t.string   "day",          limit: 255
    t.integer  "course_id",    limit: 4
    t.integer  "teacher_id",   limit: 4
    t.time     "start_hour"
    t.time     "end_hour"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "timetable_id", limit: 4
  end

  create_table "timetables", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "day",                limit: 255
    t.string   "hour",               limit: 255
    t.integer  "sections_degree_id", limit: 4
  end

  create_table "user_mailers", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255,   default: "", null: false
    t.string   "encrypted_password",     limit: 255,   default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "roles_mask",             limit: 4
    t.integer  "state",                  limit: 4
    t.string   "type",                   limit: 255
    t.string   "first_name",             limit: 255
    t.string   "second_name",            limit: 255
    t.string   "paternal_surname",       limit: 255
    t.string   "maternal_surname",       limit: 255
    t.date     "date_of_birth"
    t.string   "last_grade",             limit: 255
    t.string   "dni",                    limit: 255
    t.boolean  "pay",                    limit: 1
    t.string   "phone",                  limit: 255
    t.text     "address",                limit: 65535
    t.integer  "sections_degree_id",     limit: 4
    t.string   "owner_email",            limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
